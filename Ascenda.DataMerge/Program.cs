﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ascenda.DataMerge
{
    [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
    public static class Program
    {
        public static Task Main(string[] args)
        {
            return CreateWebHostBuilder(args).Build().RunAsync();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel(Options)
                .UseIIS()
                .UseStartup<Startup>()
                .ConfigureKestrel(Options)
                .ConfigureLogging(ConfigureLogging);
        }

        private static void ConfigureLogging(WebHostBuilderContext context, ILoggingBuilder builder)
        {
            // Read Logging settings from appsettings.json and add providers.
            builder.AddConfiguration(context.Configuration.GetSection("Logging"))
                .AddConsole()
                .AddDebug();
        }

        private static void Options(KestrelServerOptions o)
        {
            o.Limits.MaxConcurrentConnections = null;
            o.Limits.MaxConcurrentUpgradedConnections = null;
            o.Limits.MaxRequestBufferSize = null;
            o.Limits.MaxRequestHeaderCount = 4096;
            o.Limits.MaxRequestHeadersTotalSize = 32768 * o.Limits.MaxRequestHeaderCount;
            // <see cref="https://stackoverflow.com/a/47112438"/> 
            o.Limits.MaxRequestBodySize = null;

            o.Limits.MaxResponseBufferSize = null;
            o.AddServerHeader = false;
        }
    }
}
