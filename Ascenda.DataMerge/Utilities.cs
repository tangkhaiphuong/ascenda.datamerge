﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ascenda.DataMerge.Entities;
using Ascenda.Net;
using Ascenda.Net.Http;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;

// ReSharper disable once CheckNamespace
namespace Ascenda.DataMerge
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "PartialTypeWithSinglePart")]
    public static partial class Utilities
    {
        /// <summary>
        /// Merge and create new hotel entity.
        /// </summary>
        /// <param name="left">The left hotel entity</param>
        /// <param name="right">The right hotel entity.</param>
        /// <param name="id">The identity of right.</param>
        /// <returns></returns>
        public static HotelEntity Merge(HotelEntity left, HotelEntity right)
        {
            if (left == null) throw new ArgumentNullException(nameof(left));

            if (right == null) throw new ArgumentNullException(nameof(right));

            var comparer = new ReferenceDataEqualityComparer();

            var result = new HotelEntity()
            {
                HotelId = Merge(left.HotelId, right.HotelId),
                Name = Merge(left.Name, right.Name),
                Description = Merge(left.Description, right.Description),
                Detail = Merge(left.Detail, right.Detail),
                Information = Merge(left.Information, right.Information),
                DestinationId = left.DestinationId ?? right.DestinationId,

                Location = new HotelEntity.LocationData
                {
                    Address = Merge(left.Location.Address, right.Location.Address),
                    City = Merge(left.Location.City, right.Location.City),
                    Country = Merge(left.Location.Country, right.Location.Country),
                    PostalCode = Merge(left.Location.PostalCode, right.Location.PostalCode),
                    Latitude = left.Location.Latitude ?? right.Location.Latitude,
                    Longitude = left.Location.Longitude ?? right.Location.Longitude
                },

                Amenities = new HotelEntity.AmenitiesData
                {
                    General = Merge(
                        left.Amenities.General?.Select(c => c?.Trim()),
                        right.Amenities.General?.Select(c => c?.Trim()),
                        StringComparer.InvariantCultureIgnoreCase),

                    Room = Merge(
                        left.Amenities.Room?.Select(c => c.Trim()),
                        right.Amenities.Room?.Select(c => c.Trim()),
                        StringComparer.InvariantCultureIgnoreCase),
                },

                Images = new HotelEntity.ImagesData()
                {
                    Site = Merge(
                        left.Images.Site,
                        right.Images.Site,
                        comparer),
       
                    Rooms = Merge(
                            left.Images.Rooms,
                            right.Images.Rooms,
                            comparer),
                        
                    Amenities = Merge(
                        left.Images.Amenities,
                        right.Images.Amenities,
                        comparer)
                },

                BookingConditions = Merge(
                    left.BookingConditions, 
                    right.BookingConditions, 
                    StringComparer.InvariantCultureIgnoreCase),
            };

            return result;
        }

        public static string Merge(string left, string right)
        {
            left = left?.Trim();

            right = right?.Trim();

            if (string.IsNullOrEmpty(left))
                return right;

            return left;
        }

        public static IEnumerable<T> Merge<T>(
            IEnumerable<T> left, 
            IEnumerable<T> right,
            IEqualityComparer<T> comparer
            )
        {
            left = left ?? Enumerable.Empty<T>();
            right = right ?? Enumerable.Empty<T>();
            return left.Concat(right).Distinct().Distinct(comparer);
        }

        public class ReferenceDataEqualityComparer : IEqualityComparer<HotelEntity.ReferenceData>
        {
            public bool Equals(HotelEntity.ReferenceData x, HotelEntity.ReferenceData y)
            {
                if (x?.Link != y?.Link) return false;

                if (x?.Description != y?.Description) return false;

                return true;
            }

            public int GetHashCode(HotelEntity.ReferenceData obj)
            {
                unchecked // Overflow is fine, just wrap
                {
                    int hash = 17;
                    // Suitable nullity checks etc, of course :)
                    hash = hash * 23 + obj.Link.GetHashCode();
                    hash = hash * 23 + obj.Description.GetHashCode();
                    return hash;
                }
            }
        }
    }
}
