﻿using System;
using System.Net.Http;
using Ascenda.DataMerge.Data;
using Ascenda.DataMerge.Entities;
using Ascenda.DataMerge.HostedServices;
using Ascenda.DataMerge.Internal;
using Ascenda.DataMerge.Miscellaneous;
using Ascenda.DataMerge.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Ascenda.Net.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Ascenda.DataMerge
{
    public partial class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
#pragma warning disable CA1822 // Mark members as static
        public void ConfigureServices(IServiceCollection services)
#pragma warning restore CA1822 // Mark members as static
        {
            services.AddMemoryCache();
            services.AddResponseCompression();
            services.AddMvc().
                SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new AsyncEnumerableConverter());
                    options.SerializerSettings.Converters.Add(new AsyncEnumerableResponseConverter(ex => ex.GetStatusCode()));
                });
            services.AddSingleton<ErrorHandlingMiddleware>();
            services.AddSingleton<DisableResponseBufferingMiddleware>();

            services.AddHttpClient();

            services.AddSingleton<HotelsRepository>(c =>
            {
                var logger = c.GetService<ILogger<HotelsRepository>>();
                var configure = c.GetService<IConfiguration>();
                var memoryCache = c.GetService<IMemoryCache>();

                var connectionString = configure.GetValue<string>("MONGO_URI") ?? configure.GetConnectionString("MongoUri");
                var collectionName = configure.GetValue<string>("MONGO_COLLECTION") ?? configure.GetSection("Settings").GetValue<string>("Collection");

                return new HotelsRepository(logger, memoryCache, connectionString, collectionName);
            });
            services.AddSingleton<DataSourceProvider>();

            services.AddSingleton<IDataStorage<HotelEntity>, HotelDataStorage>();

            services.AddSingleton(c =>
            {
                var httpClientFactory = c.GetService<IHttpClientFactory>();
                var importer = c.GetService<ILogger<Importer<HotelEntity>>>();
                var dataStorage = c.GetService<IDataStorage<HotelEntity>>();
                var configure = c.GetService<IConfiguration>();

                var currentMerge = configure.GetSection("Settings").GetValue<int>("ConcurrentMerge");

                return new Importer<HotelEntity>(
                    httpClientFactory,
                    importer,
                    dataStorage,
                    currentMerge);
            });

            services.AddHostedService<DataSourceBackgroundService<HotelEntity>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
#pragma warning disable CA1822 // Mark members as static
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
#pragma warning restore CA1822 // Mark members as static
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            else app.UseHsts();

            app.UseResponseCompression();
            app.UseHttpsRedirection();
            // https://stackoverflow.com/a/38935583
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<DisableResponseBufferingMiddleware>();
            app.UseMvc();
        }
    }
}
