﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Ascenda.DataMerge.Responses;

namespace Ascenda.DataMerge.Controllers
{
    [Route("/")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class RootController : ControllerBase
    {
        [HttpGet]
#pragma warning disable CA1822 // Mark members as static
        public IndexResponse Default()
#pragma warning restore CA1822 // Mark members as static
        {
            return new IndexResponse();
        }
    }
}
