﻿using System.Collections.Generic;
using System.Linq;
using Ascenda.DataMerge.Data;
using Ascenda.DataMerge.Entities;
using Ascenda.DataMerge.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Ascenda.DataMerge.Controllers
{
    [Route("/hotels")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HotelController : ControllerBase
    {
        private readonly HotelsRepository _repository;

        public HotelController(HotelsRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IAsyncEnumerable<HotelResponse> GetAsyncHotels(
            [FromQuery(Name = "hotel-id")]IEnumerable<string> hotelIds,
            [FromQuery(Name = "destination")] int? destination)
        {
            return _repository.FindAsyncHotels(hotelIds, destination).Select(Convert);
        }

        private static HotelResponse Convert(HotelEntity c)
        {
            return new HotelResponse
            {
                Id = c.HotelId,
                Name = c.Name,
                DestinationId = c.DestinationId,
                BookingConditions = c.BookingConditions,
                Description = c.Information ?? c.Detail ?? c.Description,
                Images = new HotelResponse.ImagesData()
                {
                    Amenities = c.Images.Amenities.Select(d => new HotelResponse.ReferenceData { Link = d.Link, Description = d.Description }),
                    Rooms = c.Images.Rooms.Select(d => new HotelResponse.ReferenceData { Link = d.Link, Description = d.Description }),
                    Site = c.Images.Site.Select(d => new HotelResponse.ReferenceData { Link = d.Link, Description = d.Description }),
                },
                Location = new HotelResponse.LocationData()
                {
                    Address = c.Location.Address,
                    City = c.Location.City,
                    Country = c.Location.Country,
                    Latitude = c.Location.Latitude,
                    Longitude = c.Location.Longitude
                },
                Amenities = new HotelResponse.AmenitiesData()
                {
                    General = c.Amenities.General,
                    Room = c.Amenities.Room
                }
            };
        }
    }
}
