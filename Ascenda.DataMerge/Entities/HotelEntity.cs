﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using Newtonsoft.Json;

namespace Ascenda.DataMerge.Entities
{
    [BsonIgnoreExtraElements]
    public class HotelEntity
    {
        [BsonId(IdGenerator = typeof(ObjectIdGenerator))]
        [BsonElement("_id")]
        public ObjectId Id { get; set; }

        [BsonElement("hotelId")]
        [JsonProperty("hotelId")]
        public string HotelId { get; set; }

        [BsonElement("destinationId")]
        [JsonProperty("destinationId")]
        public int? DestinationId { get; set; }

        [BsonElement("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [BsonElement("location")]
        [JsonProperty("location")]
        public LocationData Location { get; set; }

        [BsonElement("description")]
        [JsonProperty("description")]
        public string Description { get; set; }
        
        [BsonElement("information")]
        [JsonProperty("information")]
        public string Information { get; set; }

        [BsonElement("detail")]
        [JsonProperty("detail")]
        public string Detail { get; set; }

        [BsonElement("amenities")]
        [JsonProperty("amenities")]
        public AmenitiesData Amenities { get; set; }

        [BsonElement("images")]
        [JsonProperty("images")]
        public ImagesData Images { get; set; }

        [BsonElement("bookingConditions")]
        [JsonProperty("bookingConditions")]
        public IEnumerable<string> BookingConditions { get; set; }

        [BsonElement("session")]
        [JsonProperty("session")]
        public string Session { get; set; }

        public partial class AmenitiesData
        {
            [BsonElement("general")]
            [JsonProperty("general")]
            public IEnumerable<string> General { get; set; }

            [BsonElement("room")]
            [JsonProperty("room")]
            public IEnumerable<string> Room { get; set; }
        }

        public partial class ImagesData
        {
            [BsonElement("rooms")]
            [JsonProperty("rooms")]
            public IEnumerable<ReferenceData> Rooms { get; set; }

            [BsonElement("site")]
            [JsonProperty("site")]
            public IEnumerable<ReferenceData> Site { get; set; }

            [BsonElement("amenities")]
            [JsonProperty("amenities")]
            public IEnumerable<ReferenceData> Amenities { get; set; }
        }

        public partial class ReferenceData
        {
            [BsonElement("link")]
            [JsonProperty("link")]
            public Uri Link { get; set; }

            [BsonElement("description")]
            [JsonProperty("description")]
            public string Description { get; set; }
        }

        public partial class LocationData
        {
            [BsonElement("latitude")]
            [JsonProperty("latitude")]
            public double? Latitude { get; set; }

            [BsonElement("longitude")]
            [JsonProperty("longitude")]
            public double? Longitude { get; set; }

            [BsonElement("address")]
            [JsonProperty("address")]
            public string Address { get; set; }

            [BsonElement("city")]
            [JsonProperty("city")]
            public string City { get; set; }

            [BsonElement("country")]
            [JsonProperty("country")]
            public string Country { get; set; }

            [BsonElement("postalCode")]
            [JsonProperty("postalCode")]
            public string PostalCode { get; set; }
        }
    }
}
