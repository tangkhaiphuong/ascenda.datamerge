﻿using System;

namespace Ascenda.DataMerge.Data
{
    public class DataSource
    {
        /// <summary>
        /// Gets or sets transformer url.
        /// </summary>
        public Uri TransformerUrl { get; set; }

        /// <summary>
        /// Gets or sets source url.
        /// </summary>
        public Uri SourceUrl { get; set; }
    }
}
