﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using MongoDB.Driver;
using Ascenda.Collections;
using Ascenda.DataMerge.Entities;
using Microsoft.Extensions.Logging;

// ReSharper disable once IdentifierTypo
namespace Ascenda.DataMerge.Repositories
{
    [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
    public class HotelsRepository
    {
        private static readonly Collation Collation = new Collation("en", strength: CollationStrength.Secondary);

        private static readonly FindOptions<HotelEntity, HotelEntity> FindOptions = new FindOptions<HotelEntity, HotelEntity> { Collation = Collation };

        private static readonly FindOneAndReplaceOptions<HotelEntity, HotelEntity> FindOneAndUpdateOptions = new FindOneAndReplaceOptions<HotelEntity, HotelEntity> { Collation = Collation };

        private static readonly CreateIndexModel<BsonDocument> HotelIdIndex = new CreateIndexModel<BsonDocument>(new BsonDocument("hotelId", 1), new CreateIndexOptions<BsonDocument>
        { Unique = true, Name = "hotelId", Collation = Collation });

        private static readonly CreateIndexModel<BsonDocument> DestinationIdIndex = new CreateIndexModel<BsonDocument>(new BsonDocument("destinationId", 1), new CreateIndexOptions<BsonDocument>
        { Unique = false, Name = "destinationId", Collation = Collation });


        private readonly ILogger<HotelsRepository> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly string _collectionName;
        private readonly MongoUrl _mongoUrl;

        /// <inheritdoc />
        public HotelsRepository(
            ILogger<HotelsRepository> logger,
            IMemoryCache memoryCache,
            string connectionString,
            string collectionName)
        {
            _logger = logger;
            _memoryCache = memoryCache;
            _collectionName = collectionName;
            _mongoUrl = new MongoUrl(connectionString);
        }

        /// <summary>
        /// Find hooks.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="hotelIds"></param>
        /// <returns></returns>
        public IAsyncEnumerable<HotelEntity> FindAsyncHotels(
            IEnumerable<string> hotelIds,
            int? destination,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return new AsyncEnumerable<HotelEntity>(async yield =>
            {
                var collection = await GetMongoCollectionAsync().ConfigureAwait(false);

                var idConditions = hotelIds.Select(c => Builders<HotelEntity>.Filter.Eq(d => d.HotelId, c));

                if (destination != null)
                    idConditions = idConditions.Concat(new[] { Builders<HotelEntity>.Filter.Eq(d => d.DestinationId, destination) });

                var filter = Builders<HotelEntity>.Filter.Or(idConditions);

                using (var customers = await collection.FindAsync(filter, FindOptions, cancellationToken)
                        .ConfigureAwait(false))
                {
                    await customers.ToAsyncEnumerable(cancellationToken)
                        .YieldAsync(yield, cancellationToken).ConfigureAwait(false);
                }
            });
        }

        /// <summary>
        /// Merge hotel.
        /// </summary>
        /// <param name="hotel">The hotel instance.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<HotelEntity> MergeAsync(
            HotelEntity hotel,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var collection = await GetMongoCollectionAsync().ConfigureAwait(false);

            var filter = Builders<HotelEntity>.Filter.Eq(c => c.HotelId, hotel.HotelId);

            var existedHotel = await collection.FindSync(filter, FindOptions, cancellationToken).FirstOrDefaultAsync(cancellationToken).ConfigureAwait(false);

            // Hotel doesn't exist. Insert new
            if (existedHotel == null)
            {
                await collection.InsertOneAsync(hotel, cancellationToken: cancellationToken).ConfigureAwait(false);
                return hotel;
            }

            // Merge to this.
            var newHotel = Utilities.Merge(existedHotel, hotel);
            newHotel.Id = existedHotel.Id;
            newHotel.Session = hotel.Session;

            newHotel = await collection.FindOneAndReplaceAsync(filter, newHotel, FindOneAndUpdateOptions, cancellationToken).ConfigureAwait(false);

            return newHotel;
        }

        public async Task<long> RemoveDiffSessionAsync(string session, CancellationToken cancellationToken)
        {
            var collection = await GetMongoCollectionAsync().ConfigureAwait(false);

            var filter = Builders<HotelEntity>.Filter.Ne(c => c.Session, session);

            var existedHotel = await collection.DeleteManyAsync(filter, cancellationToken: cancellationToken).ConfigureAwait(false);

            return existedHotel.DeletedCount;
        }

        private async Task<IMongoCollection<HotelEntity>> GetMongoCollectionAsync()
        {
            string key = $"{nameof(HotelsRepository)}.{nameof(GetMongoCollectionAsync)}(" + _mongoUrl.DatabaseName + ")";

            return (await _memoryCache.GetOrCreateAsync(key, async (_) =>
            {
                _logger.LogInformation($"Creating connection to \"{_mongoUrl}\" with collection \"{_collectionName}\"");

                var orCreate = new MongoClient(_mongoUrl);

                var db = orCreate.GetDatabase(_mongoUrl.DatabaseName);

                try
                {
                    var collection = db.GetCollection<BsonDocument>(_collectionName);
                    await collection.Indexes.CreateManyAsync(new[] { HotelIdIndex, DestinationIdIndex }).ConfigureAwait(false);
                }
                catch (TaskCanceledException)
                {
                    throw;
                }
                catch (Exception)
                {
                    /* Skip */
                }

                return db;

            }).ConfigureAwait(continueOnCapturedContext: false)).GetCollection<HotelEntity>(_collectionName);
        }
    }
}