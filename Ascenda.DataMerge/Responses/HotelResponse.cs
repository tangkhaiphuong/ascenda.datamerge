﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ascenda.DataMerge.Data
{
    public partial class HotelResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("destination_id")]
        public int? DestinationId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("location")]
        public LocationData Location { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("amenities")]
        public AmenitiesData Amenities { get; set; }

        [JsonProperty("images")]
        public ImagesData Images { get; set; }

        [JsonProperty("bookingConditions")]
        public IEnumerable<string> BookingConditions { get; set; }

        public partial class AmenitiesData
        {
            [JsonProperty("general")]
            public IEnumerable<string> General { get; set; }

            [JsonProperty("room")]
            public IEnumerable<string> Room { get; set; }
        }

        public partial class ImagesData
        {
            [JsonProperty("rooms")]
            public IEnumerable<ReferenceData> Rooms { get; set; }

            [JsonProperty("site")]
            public IEnumerable<ReferenceData> Site { get; set; }

            [JsonProperty("amenities")]
            public IEnumerable<ReferenceData> Amenities { get; set; }
        }

        public partial class ReferenceData
        {
            [JsonProperty("link")]
            public Uri Link { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }
        }

        public partial class LocationData
        {
            [JsonProperty("lat")]
            public double? Latitude { get; set; }

            [JsonProperty("lng")]
            public double? Longitude { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("city")]
            public string City { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }
        }
    }
}
