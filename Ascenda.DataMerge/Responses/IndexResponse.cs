﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ascenda.DataMerge.Responses
{
    [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
    public class IndexResponse
    {
        private static readonly string HostName = Environment.GetEnvironmentVariable("HOST_HOSTNAME");
        private static readonly Process Process = Process.GetCurrentProcess();

        [JsonProperty("statusCode", Order = 1)]
        public long StatusCode { get; } = 200;

        [JsonProperty("message", Order = 2)]
        public string Message { get; } = "Hello!";

        [JsonProperty("name", Order = 3)]
        public string Name { get; } = "ascenda-data-merge";

        [JsonProperty("server", Order = 4)]
        [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
        public string Server => string.IsNullOrEmpty(HostName) ? Environment.MachineName : HostName;

        [JsonProperty("description", Order = 5)]
        public string Description { get; } = "Ascenda DataMerge";

        [JsonProperty("version", Order = 6)]
        public string Version { get; } = "1.0.0";

        [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
        [JsonProperty("releasedDate", ItemConverterType = typeof(IsoDateTimeConverter), Order = 7)]
        public DateTimeOffset ReleaseDate
        {
            get
            {
                var vi = FileVersionInfo.GetVersionInfo(typeof(IndexResponse).Assembly.Location);
                var fileInfo = new FileInfo(vi.FileName);
                var createTime = fileInfo.LastWriteTimeUtc;
                return createTime;
            }
        }

        [JsonProperty("startedAt", ItemConverterType = typeof(IsoDateTimeConverter), Order = 8)]
        public DateTimeOffset StartedAt { get; } = Process.StartTime;

        [JsonProperty("upTime", Order = 9)]
        public string UpTime => (DateTimeOffset.Now - StartedAt).ToString();

        [JsonProperty("now", Order = 10)]
        public DateTimeOffset Now { get; } = DateTime.UtcNow;

        [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
        [JsonProperty("system", Order = 11)]
        public object System => new
        {
            architect = RuntimeInformation.ProcessArchitecture.ToString(),
            platform = RuntimeInformation.OSDescription,
            framework = RuntimeInformation.FrameworkDescription,
            cpus = Environment.ProcessorCount,
        };

        [JsonProperty("data", Order = 12)]
        public object Data { get; set; }
    }
}