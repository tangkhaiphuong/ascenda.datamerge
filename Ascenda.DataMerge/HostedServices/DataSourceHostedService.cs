﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Ascenda.Collections;
using Ascenda.DataMerge.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Ascenda.DataMerge.HostedServices
{
    [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
    [SuppressMessage("ReSharper", "CatchAllClause")]
    [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
    [SuppressMessage("ReSharper", "EventExceptionNotDocumented")]
    internal class DataSourceBackgroundService<T> : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly Importer<T> _importer;
        private readonly IDataStorage<T> _dataStorage;
        private readonly DataSourceProvider _dataSourceProvider;
        private readonly IConfiguration _configure;

        /// <summary>
        /// Construct data host service.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="importer"></param>
        /// <param name="dataSourceProvider"></param>
        /// <param name="configure"></param>
        public DataSourceBackgroundService(
            ILogger<DataSourceBackgroundService<T>> logger,
            Importer<T> importer,
            IDataStorage<T> dataStorage,
            DataSourceProvider dataSourceProvider,
            IConfiguration configure)
        {
            _logger = logger;
            _importer = importer;
            _dataStorage = dataStorage;
            _dataSourceProvider = dataSourceProvider;
            _configure = configure;
        }

        /// <inheritdoc />
        [SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var currentFetch = _configure.GetSection("Settings").GetValue<int>("ConcurrentFetch");
            var interval = _configure.GetSection("Settings").GetValue<TimeSpan>("IntervalFetch");

            var dataSource = _dataSourceProvider.Get();

            while (cancellationToken.IsCancellationRequested == false)
            {
                var session = Guid.NewGuid().ToString();
                // Run many data source at same time.
                var taskBatches = dataSource.Select(async item =>
                {
                    try
                    {
                        var stopWatch = Stopwatch.StartNew();
                        _logger.LogInformation($"Starting import data source {item.SourceUrl}");

                        await _importer.ExecuteAsync(item, session, cancellationToken).ConfigureAwait(false);

                        _logger.LogInformation($"Completing import data source {item.SourceUrl}. ({stopWatch.Elapsed})");
                    }
                    catch (TaskCanceledException)
                    {
                        return;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Failing import data source \"{item.SourceUrl}\" due {ex.Message}");
                    }
                }).Batch(currentFetch, true);

                // Run import parallel
                foreach (var tasks in taskBatches)
                    foreach (var task in tasks)
                        await task.ConfigureAwait(false);

                // Clean old hotel.
                await _dataStorage.RemoveDiffSessionAsync(session, cancellationToken).ConfigureAwait(false);

                await Task.Delay(interval, cancellationToken).ConfigureAwait(false);
            }
        }
    }
}