﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using Ascenda.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

// ReSharper disable once IdentifierTypo
namespace Ascenda.DataMerge.Miscellaneous
{
    /// <see>
    ///     <cref>https://stackoverflow.com/a/38935583</cref>
    /// </see>
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
    public class ErrorHandlingMiddleware : IMiddleware
    {
        private readonly IHostingEnvironment _env;

        /// <inheritdoc />
        public ErrorHandlingMiddleware(IHostingEnvironment env) => _env = env;

        /// <summary>
        /// Get status code from exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns></returns>
        [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
        private Task<(HttpStatusCode StatusCode, bool IsTrace)> GetStatusCodeAsync(Exception ex)
        {
            var result = ex.GetStatusCode(out var trace);
            return Task.FromResult((result, trace));
        }

        /// <inheritdoc />
        [SuppressMessage("ReSharper", "CatchAllClause")]
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var stopWatch = Stopwatch.StartNew();
            try
            {
                if (next != null) await next(context).ConfigureAwait(false);
                stopWatch.Stop();
            }

            catch (TaskCanceledException)
            {
                throw; // Don't handler task cancel exception.
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
            {
                stopWatch.Stop();
                await HandleExceptionAsync(context, ex, stopWatch.Elapsed).ConfigureAwait(false);
            }
#pragma warning restore CA1031 // Do not catch general exception types
        }

        [SuppressMessage("ReSharper", "CatchAllClause")]
        private async Task HandleExceptionAsync(HttpContext context, Exception exception, TimeSpan elapsed)
        {
            if (context.Response.HasStarted) return;

            var (statusCode, isTrace) = await GetStatusCodeAsync(exception).ConfigureAwait(false);

            var result = exception.CreateResponse<Response>(statusCode, isTrace, elapsed);

            var response = JsonConvert.SerializeObject(result, _env.IsDevelopment() ? Formatting.Indented : Formatting.None);

            context.Response.ContentLength = response.Length;
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)result.StatusCode;
            if (!string.IsNullOrEmpty(result.Error))
            {
                try
                {
                    var text = result.Error.Replace("\r", " ", StringComparison.OrdinalIgnoreCase).Replace("\n", " ", StringComparison.OrdinalIgnoreCase);
                    text = string.Join("", System.Text.Encoding.ASCII.GetChars(System.Text.Encoding.ASCII.GetBytes(text.ToCharArray())));
                    context.Response.Headers.Add("Error", new Microsoft.Extensions.Primitives.StringValues(text));
                    context.Response.Headers.Add("Warning", new Microsoft.Extensions.Primitives.StringValues((int)result.StatusCode + " - " + "\"" + text + "\""));
                }
#pragma warning disable CA1031 // Do not catch general exception types
                catch
                {
                    // ignored
                }
#pragma warning restore CA1031 // Do not catch general exception types
            }

            await context.Response.WriteAsync(response).ConfigureAwait(false);
        }
    }
}