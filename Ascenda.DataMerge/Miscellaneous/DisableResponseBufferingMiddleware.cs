﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

// ReSharper disable once IdentifierTypo
namespace Ascenda.DataMerge.Miscellaneous
{
    /// <see>
    /// <cref>https://stackoverflow.com/a/47163755</cref>
    /// <cref>https://www.tpeczek.com/2017/08/aspnet-core-response-compression.html</cref>
    /// </see>
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class DisableResponseBufferingMiddleware : IMiddleware
    {
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="next"/> is <see langword="null"/></exception>
        public Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            if (context == null) throw new ArgumentNullException(nameof(next));

            var bufferingFeature = context.Features.Get<IHttpBufferingFeature>();
            bufferingFeature?.DisableResponseBuffering();

            return next?.Invoke(context);
        }
    }
}
