﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Ascenda.Collections;
using Ascenda.DataMerge.Data;
using JUST;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AsyncEnumerable = System.Linq.AsyncEnumerable;

namespace Ascenda.DataMerge.Internal
{
    public class Importer<T>
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger _logger;
        private readonly IDataStorage<T> _dataStorage;
        private readonly int _concurrence;

        public Importer(
            IHttpClientFactory httpClientFactory,
            ILogger<Importer<T>> logger,
            IDataStorage<T> dataStorage,
            int concurrence)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _dataStorage = dataStorage;
            _concurrence = concurrence;
        }

        /// <summary>
        /// Executing import data.
        /// </summary>
        /// <param name="dataSource">Data source.</param>
        /// <param name="session">The session.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"><paramref name="dataSource"/> is <see langword="null"/></exception>
        public async Task ExecuteAsync(
            DataSource dataSource, 
            string session, 
            CancellationToken cancellationToken)
        {
            if (dataSource == null) throw new ArgumentNullException(nameof(dataSource));

            // Get transformer
            var transformer = await GetTransformerAsync(dataSource.TransformerUrl, cancellationToken).ConfigureAwait(false);

            // Get async item in stream.
            var asyncItems = GetAsyncSource(dataSource.SourceUrl, cancellationToken)
                .Select(hotel => JsonTransformer.Transform(transformer, hotel.ToString(), new JUSTContext()))
                .Select(JsonConvert.DeserializeObject<T>);

            // Perform save in batch.
            var taskBatches = asyncItems.Select(async hotel =>
            {
                try
                {
                    await _dataStorage.SaveAsync(hotel, session, cancellationToken).ConfigureAwait(false);
                }
                catch (TaskCanceledException)
                {
                    /* Skip */
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex,
                        $"Failing save object ${JsonConvert.SerializeObject(hotel)} due {ex.Message}");
                }
            }).Batch(_concurrence, true, cancellationToken);

            var enumerator = taskBatches.GetAsyncEnumerator(cancellationToken);

            try
            {
                while (await enumerator.MoveNextAsync().ConfigureAwait(false))
                {
                    var interEnumerator = enumerator.Current.GetAsyncEnumerator(cancellationToken);
                    try
                    {
                        while (await interEnumerator.MoveNextAsync().ConfigureAwait(false))
                            await interEnumerator.Current.ConfigureAwait(false);
                    }
                    finally
                    {
                        await interEnumerator.DisposeAsync().ConfigureAwait(false);
                    }
                }
            }
            finally
            {
                await enumerator.DisposeAsync().ConfigureAwait(false);
            }
        }

        private async Task<string> GetTransformerAsync(Uri url, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (url.IsFile)
            {
                var localPath = url.LocalPath;
                var filePath = Path.Join(Environment.CurrentDirectory, localPath);
                if (File.Exists(filePath))
                {
                    var content = await File.ReadAllTextAsync(filePath, cancellationToken).ConfigureAwait(false);
                    return content;
                }
                else
                {
                    var content = await File.ReadAllTextAsync(localPath, cancellationToken).ConfigureAwait(false);
                    return content;
                }
            }

            using (var httpClient = _httpClientFactory.CreateClient())
            {
                var payload = await httpClient.GetStringAsync(url)
                    .ConfigureAwait(false);

                return payload;
            }
        }

        private IAsyncEnumerable<JToken> GetAsyncSource(Uri url, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!url.IsFile)
                return new AsyncEnumerable<JToken>(async yield =>
                {
                    using (var httpClient = _httpClientFactory.CreateClient())
                    using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                    using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                        .ConfigureAwait(false))
                    {
                        using (var content = response.Content)
                        using (var stream = await content.ReadAsStreamAsync().ConfigureAwait(false))
                            await GenerateAsync(stream, yield, cancellationToken).ConfigureAwait(false);
                    }
                });

            var localPath = url.LocalPath;
            var filePath = Path.Join(Environment.CurrentDirectory, localPath);

            if (File.Exists(filePath))
            {
                return new AsyncEnumerable<JToken>(async yield =>
                  {
                      using (var stream = File.OpenRead(filePath))
                          await GenerateAsync(stream, yield, cancellationToken).ConfigureAwait(false);
                  });
            }

            return new AsyncEnumerable<JToken>(async yield =>
            {
                using (var stream = File.OpenRead(localPath))
                    await GenerateAsync(stream, yield, cancellationToken).ConfigureAwait(false);
            });
        }

        private static async Task GenerateAsync(Stream stream, AsyncEnumerable<JToken>.Yield yield, CancellationToken cancellationToken)
        {
            using (var streamReader = new StreamReader(stream))
            using (var reader = new JsonTextReader(streamReader) { SupportMultipleContent = true })
            {
                cancellationToken.ThrowIfCancellationRequested();

                var canRead = await reader.ReadAsync(cancellationToken).ConfigureAwait(false);

                if (!canRead) return;

                if (reader.TokenType != JsonToken.StartArray)
                    return;

                var jsonSerializer = JsonSerializer.Create();

                while (await reader.ReadAsync(cancellationToken).ConfigureAwait(false))
                {
                    if (reader.TokenType != JsonToken.StartObject) break;

                    var item = jsonSerializer.Deserialize<JObject>(reader);

                    await yield.ReturnAsync(item).ConfigureAwait(false);
                }
            }
        }
    }
}