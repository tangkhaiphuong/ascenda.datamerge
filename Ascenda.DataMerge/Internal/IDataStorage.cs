﻿using System.Threading;
using System.Threading.Tasks;

namespace Ascenda.DataMerge.Internal
{
    /// <summary>
    /// Data storage interface.
    /// </summary>
    /// <typeparam name="T">The data type.</typeparam>
    public interface IDataStorage<in T>
    {
        /// <summary>
        /// Save data.
        /// </summary>
        /// <param name="data">The data instance.</param>
        /// <param name="session">The session.</param>
        /// <param name="cancellationToken">Then cancellation token.</param>
        /// <returns></returns>
        Task SaveAsync(T data, string session, CancellationToken cancellationToken);

        /// <summary>
        /// Remove by diff session..
        /// </summary>
        /// <param name="session">The session item.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task RemoveDiffSessionAsync(string session, CancellationToken cancellationToken);
    }
}