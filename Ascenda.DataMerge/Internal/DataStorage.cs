﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Ascenda.DataMerge.Entities;
using Ascenda.DataMerge.Repositories;

namespace Ascenda.DataMerge.Internal
{
    // Hotel data storage.
    public class HotelDataStorage : IDataStorage<HotelEntity>
    {
        private readonly ConcurrentDictionary<string, SemaphoreSlim> _semaphoreSlimRepo = new ConcurrentDictionary<string, SemaphoreSlim>();

        private readonly HotelsRepository _respository;

        public HotelDataStorage(HotelsRepository respository)
        {
            _respository = respository;
        }

        /// <inheritdoc />
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="initialCount">initialCount</paramref> is less than 0.</exception>
        /// <exception cref="OverflowException">The dictionary already contains the maximum number of elements (<see cref="System.Int32.MaxValue"></see>).</exception>
        /// <exception cref="ArgumentNullException"><paramref name="entity"/> is <see langword="null"/></exception>
        /// <exception cref="OperationCanceledException"><paramref name="cancellationToken">cancellationToken</paramref> was canceled.</exception>
        /// <exception cref="SemaphoreFullException">The <see cref="System.Threading.SemaphoreSlim"></see> has already reached its maximum size.</exception>
        public async Task SaveAsync(
            HotelEntity entity, string
            session,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            var semaphore = new SemaphoreSlim(1);

            semaphore = _semaphoreSlimRepo.GetOrAdd(entity.HotelId, semaphore);

            await semaphore.WaitAsync(cancellationToken).ConfigureAwait(false);

            try
            {
                entity.Session = session;
                await _respository.MergeAsync(entity, cancellationToken).ConfigureAwait(false);
            }
            finally
            {
                semaphore.Release();
            }
        }

        public Task RemoveDiffSessionAsync(string session, CancellationToken cancellationToken)
        {
            return _respository.RemoveDiffSessionAsync(session, cancellationToken);
        }
    }
}