﻿using System.Collections.Generic;
using Ascenda.DataMerge.Data;
using Microsoft.Extensions.Configuration;

namespace Ascenda.DataMerge.Internal
{
    /// <summary>
    /// Data source presentation.
    /// </summary>
    public class DataSourceProvider
    {
        private readonly IConfiguration _configure;

        /// <summary>
        /// Construct data source provider.
        /// </summary>
        /// <param name="configure"></param>
        public DataSourceProvider(
            IConfiguration configure)
        {
            _configure = configure;
        }

        /// <summary>
        /// Gets data source collections.
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<DataSource> Get()
        {
            return _configure.GetSection("DataSources").Get<List<DataSource>>();
        }
    }
}