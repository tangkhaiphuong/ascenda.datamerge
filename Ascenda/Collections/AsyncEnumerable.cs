﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Collections
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public static partial class AsyncEnumerable
    {
        /// <summary>
        /// Yield values.
        /// </summary>
        /// <param name="source">The source data.</param>
        /// <param name="yield">Yield producer.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/></exception>
        public static async Task<ulong> YieldAsync<T>(this IAsyncEnumerable<T> source, AsyncEnumerable<T>.Yield yield, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (yield == null) throw new ArgumentNullException(nameof(yield));

            var enumerator = source.GetAsyncEnumerator(cancellationToken);
            try
            {
                ulong count;
                for (count = 0; await enumerator.MoveNextAsync().ConfigureAwait(false); ++count)
                    await yield.ReturnAsync(enumerator.Current).ConfigureAwait(false);

                return count;
            }
            finally
            {
                await enumerator.DisposeAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Yield values.
        /// </summary>
        /// <param name="yield">Yield producer.</param>
        /// <param name="source">The source data.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/></exception>
        /// <exception cref="OperationCanceledException">The token has had cancellation requested.</exception>
        /// <exception cref="ObjectDisposedException">The associated <see cref="System.Threading.CancellationTokenSource"></see> has been disposed.</exception>
        public static async Task<ulong> YieldAsync<T>(this IEnumerable<T> source, AsyncEnumerable<T>.Yield yield, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (yield == null) throw new ArgumentNullException(nameof(yield));

            ulong count = 0;
            foreach (var item in source)
            {
                cancellationToken.ThrowIfCancellationRequested();
                ++count;
                await yield.ReturnAsync(item).ConfigureAwait(false);
            }

            return count;
        }
    }

    /// <inheritdoc />
    public class AsyncEnumerable<T> : IAsyncEnumerable<T>
    {
        private readonly Func<Yield, Task> _yield;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="yield">Yield factory method.</param>
        public AsyncEnumerable(Func<Yield, Task> yield) => _yield = yield;

        /// <inheritdoc />
        public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = default(CancellationToken)) => new Enumerator(_yield);

#pragma warning disable CA1034 // Nested types should not be visible
        /// <summary>
        /// Yield producer.
        /// </summary>
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public abstract class Yield
#pragma warning restore CA1034 // Nested types should not be visible
        {
            /// <summary>
            /// Return value.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <returns></returns>
            public abstract Task ReturnAsync(T value);

            /// <summary>
            /// Break value.
            /// </summary>
            /// <returns></returns>
            public abstract Task BreakAsync();
        }

        private class Enumerator : Yield, IAsyncEnumerator<T>
        {
            private readonly Func<Yield, Task> _yield;
            private TaskCompletionSource<bool> _producerSource;
            private TaskCompletionSource<bool> _consumerSource = new TaskCompletionSource<bool>();
            private bool _isBreak;
            private Task _task;

            public Enumerator(Func<Yield, Task> yield) => _yield = yield;

            public T Current { get; private set; }

            public async ValueTask DisposeAsync()
            {
                if (_task.IsCompleted) _task.Dispose();
                await Task.Yield();
            }

            /// <exception cref="Exception">A delegate callback throws an exception.</exception>
            /// <exception cref="ArgumentNullException">The <paramref>continuationAction
            ///     <name>continuationAction</name>
            /// </paramref> argument is null.</exception>
            /// <exception cref="ObjectDisposedException">The <see cref="System.Threading.CancellationTokenSource"></see> that created the token has already been disposed.</exception>
            public async ValueTask<bool> MoveNextAsync()
            {
                if (_producerSource != null) _producerSource.TrySetResult(true);

#pragma warning disable CA2008 // Do not create tasks without passing a TaskScheduler
                else _task = _yield(this).ContinueWith(OnCompleted, CancellationToken.None);
#pragma warning restore CA2008 // Do not create tasks without passing a TaskScheduler

                var result = await _consumerSource.Task.ConfigureAwait(false);

                _consumerSource = new TaskCompletionSource<bool>();

                return result;
            }

            private void OnCompleted(Task task)
            {
                if (task.Exception != null)
                {
                    var exception = task.Exception.InnerException ?? task.Exception;
                    _consumerSource.TrySetException(exception);
                    _producerSource?.TrySetException(exception);
                    return;

                }
                if (task.IsCanceled)
                {
                    _consumerSource.TrySetException(new TaskCanceledException());
                    _producerSource?.TrySetException(new TaskCanceledException());
                    return;
                }

                _isBreak = true;

                _consumerSource.TrySetResult(false);
            }

            public override async Task BreakAsync()
            {
                _isBreak = true;

                _consumerSource.TrySetResult(false);

                await Task.Yield();
            }

            public override async Task ReturnAsync(T value)
            {
                if (_isBreak)
                {
                    _consumerSource.TrySetResult(false);

                    await Task.Yield();
                }

                Current = value;

                _producerSource = new TaskCompletionSource<bool>();

                _consumerSource.TrySetResult(true);

                await _producerSource.Task.ConfigureAwait(false);
            }
        }
    }
}
