﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Collections
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "PartialTypeWithSinglePart")]
    public static partial class Utilities
    {
        /// <summary>
        /// Batches the source sequence into sized buckets.
        /// </summary>
        /// <typeparam name="T">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="size">Size of buckets.</param>
        /// <param name="buffered">Buffered batch.</param>
        /// <returns>A sequence of equally sized buckets containing elements of the source collection.</returns>
        /// <remarks>
        /// This operator uses deferred execution and streams its results (buckets and bucket content).
        /// </remarks>
        /// <exception cref="System.ArgumentNullException"><paramref name="source"/> is <see langword="null"/></exception>
        /// <exception cref="System.ArgumentException">batch must great than 0</exception>
        /// <see>
        ///     <cref>https://stackoverflow.com/a/11775295</cref>
        /// </see>
        public static IEnumerable<IEnumerable<T>> Batch<T>(
            this IEnumerable<T> source,
            int size,
            bool buffered = false)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            if (size <= 0) throw new ArgumentException("Size must great than 0", nameof(size));

            if (buffered == false)
                using (var enumerator = source.GetEnumerator())
                {
                    IEnumerable<T> GetPart(IEnumerator<T> innerEnumerator, int innerSize)
                    {
                        do
                        {
                            yield return innerEnumerator.Current;
                        } while (--innerSize > 0 && innerEnumerator.MoveNext());
                    }

                    while (enumerator.MoveNext())
                    {
                        yield return GetPart(enumerator, size);
                    }
                }
            else
            {
                var batch = new List<T>(size);
                foreach (var item in source)
                {
                    batch.Add(item);
                    if (batch.Count == size)
                    {
                        yield return batch;
                        batch = new List<T>(size);
                    }
                }

                if (batch.Count > 0)
                    yield return batch;
            }
        }
    }
}
