﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Collections
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "PartialTypeWithSinglePart")]
    public static partial class Utilities
    {
        /// <summary>
        /// Batches the source sequence into sized buckets.
        /// </summary>
        /// <typeparam name="T">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="size">Size of buckets.</param>
        /// <param name="buffered">Buffered batch.</param>
        /// <returns>A sequence of equally sized buckets containing elements of the source collection.</returns>
        /// <remarks>
        /// This operator uses deferred execution and streams its results (buckets and bucket content).
        /// </remarks>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentException">batch must great than 0</exception>
        /// <see cref="https://stackoverflow.com/a/11775295"/> 
        public static IAsyncEnumerable<IAsyncEnumerable<T>> Batch<T>(
            this IAsyncEnumerable<T> source,
            int size,
            bool buffered = false,
            [EnumeratorCancellation] CancellationToken cancellationToken = default(CancellationToken))
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            if (size <= 0) throw new ArgumentException("Size must great than 0", nameof(size));

            if (buffered == false)

                return new AsyncEnumerable<IAsyncEnumerable<T>>(async yield =>
                {
                    var enumerator = source.GetAsyncEnumerator(cancellationToken);
                    try
                    {
                        while (await enumerator.MoveNextAsync().ConfigureAwait(false))
                        {
                            var size_ = size;

                            await yield.ReturnAsync(new AsyncEnumerable<T>(async interYield =>
                            {
                                do
                                {
                                    await interYield.ReturnAsync(enumerator.Current).ConfigureAwait(false);
                                } while (--size_ > 0 && await enumerator.MoveNextAsync().ConfigureAwait(false));
                            })).ConfigureAwait(false);
                        }

                    }
                    finally
                    {
                        await enumerator.DisposeAsync().ConfigureAwait(false);
                    }
                });

            else

                return new AsyncEnumerable<IAsyncEnumerable<T>>(async yield =>
                {
                    var buffer = new List<T>(size);
                    var count = 0;
                    var enumerator = source.GetAsyncEnumerator(cancellationToken);
                    try
                    {
                        while (await enumerator.MoveNextAsync().ConfigureAwait(false))
                        {
                            if (count >= size)
                            {
                                await yield.ReturnAsync(buffer.ToAsyncEnumerable()).ConfigureAwait(false);
                                buffer = new List<T>(size);
                                count = 0;
                            }

                            buffer.Add(enumerator.Current);
                            ++count;
                        }

                        if (count != 0) await yield.ReturnAsync(buffer.ToAsyncEnumerable()).ConfigureAwait(false);
                    }
                    finally
                    {
                        await enumerator.DisposeAsync().ConfigureAwait(false);
                    }
                });
        }

        /// <summary>
        /// To async enumerable task.
        /// </summary>
        /// <typeparam name="T">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/></exception>
        public static IAsyncEnumerable<T> ToAsyncEnumerable<T>(this IAsyncEnumerable<Task<T>> source,
            [EnumeratorCancellation]  CancellationToken cancellationToken = default(CancellationToken))
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            return new AsyncEnumerable<T>(async yield =>
            {
                var enumerator = source.GetAsyncEnumerator(cancellationToken);

                try
                {
                    while (await enumerator.MoveNextAsync().ConfigureAwait(false))
                        await yield.ReturnAsync(await enumerator.Current.ConfigureAwait(false)).ConfigureAwait(false);
                }
                finally
                {
                    await enumerator.DisposeAsync().ConfigureAwait(false);
                }
            });
        }
    }
}
