﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using MongoDB.Driver;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Collections
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public static partial class AsyncEnumerable
    {
        /// <summary>
        /// Convert to Async Enumerable
        /// </summary>
        /// <typeparam name="T">Data type.</typeparam>
        /// <param name="source">Data source.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns></returns>
        public static IAsyncEnumerable<T> ToAsyncEnumerable<T>(this IAsyncCursor<T> source, CancellationToken cancellationToken = default(CancellationToken))
        {
            return new AsyncEnumerable<T>(async (yield) =>
            {
                while (await source.MoveNextAsync(cancellationToken).ConfigureAwait(false))
                    foreach (var item in source.Current)
                        await yield.ReturnAsync(item).ConfigureAwait(false);
            });
        }
    }
}
