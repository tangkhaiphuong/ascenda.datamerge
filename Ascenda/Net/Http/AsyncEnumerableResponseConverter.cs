﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Net.Http
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class AsyncEnumerableResponseConverter : JsonConverter
    {
        private readonly Func<Exception, HttpStatusCode> _httpStatusCodeFactoryMethod;

        /// <inheritdoc />
        /// <exception cref="ArgumentNullException">The http status code factory method.</exception>
        public AsyncEnumerableResponseConverter(Func<Exception, HttpStatusCode> httpStatusCodeFactoryMethod)
        {
            _httpStatusCodeFactoryMethod = httpStatusCodeFactoryMethod ?? throw new ArgumentNullException(nameof(httpStatusCodeFactoryMethod));
        }

        /// <exception cref="ArgumentNullException"><paramref name="writer"/> is <see langword="null"/></exception>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        [SuppressMessage("ReSharper", "CatchAllClause")]
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (writer == null) throw new ArgumentNullException(nameof(writer));

            var payload = value as dynamic;
            var hasItem = false;

            var printStatusCode = false;
            var printMessage = false;
            var printError = false;
            var printElapse = false;

            var stopWatch = Stopwatch.StartNew();

            writer.WriteStartObject();
            if (payload.Response != null)
            {
                var response = (Response)payload.Response;
                writer.WritePropertyName("statusCode");
                JToken.FromObject(response.StatusCode, serializer).WriteTo(writer);
                printStatusCode = true;
                if (response.Message != null)
                {
                    writer.WritePropertyName("message");
                    JToken.FromObject(response.Message, serializer).WriteTo(writer);
                    printMessage = true;
                }

                if (response.Error != null)
                {
                    writer.WritePropertyName("error");
                    JToken.FromObject(response.Error, serializer).WriteTo(writer);
                    printError = true;
                }

                if (response.Elapse != null)
                {
                    writer.WritePropertyName("elapse");
                    JToken.FromObject(stopWatch.Elapsed, serializer).WriteTo(writer);
                    printElapse = true;
                }
            }
            writer.Flush();

            try
            {
                var task = (Task)AsyncEnumerable.ForEachAsync(payload.Data, new Action<object>(c =>
                {
                    if (hasItem == false)
                    {
                        if (!printStatusCode)
                        {
                            writer.WritePropertyName("statusCode");
                            JToken.FromObject(payload.StatusCode, serializer).WriteTo(writer);
                        }
                        writer.WritePropertyName("data");
                        writer.WriteStartArray();
                    }

                    hasItem = true;
                    if (c == null)
                    {
                        writer.WriteNull();
                        writer.Flush();
                        return;
                    }

                    var t = JToken.FromObject(c, serializer);
                    t.WriteTo(writer);
                    writer.Flush();
                }));

                task.Wait();

                stopWatch.Stop();

                if (hasItem)
                {
                    writer.WriteEndArray();
                    if (payload.Message != null && !printMessage)
                    {
                        writer.WritePropertyName("message");
                        JToken.FromObject(payload.Message, serializer).WriteTo(writer);
                    }

                    if (!printElapse)
                    {
                        writer.WritePropertyName("elapse");
                        JToken.FromObject(stopWatch.Elapsed, serializer).WriteTo(writer);
                    }

                    writer.WriteEndObject();
                }
                else
                {
                    if (!printStatusCode)
                    {
                        writer.WritePropertyName("statusCode");
                        JToken.FromObject(payload.StatusCode, serializer).WriteTo(writer);
                    }

                    if (payload.Message != null && !printMessage)
                    {
                        writer.WritePropertyName("message");
                        JToken.FromObject(payload.Message, serializer).WriteTo(writer);
                    }
                    writer.WritePropertyName("data");
                    writer.WriteStartArray();
                    writer.WriteEndArray();
                    if (!printElapse)
                    {
                        writer.WritePropertyName("elapse");
                        JToken.FromObject(stopWatch.Elapsed, serializer).WriteTo(writer);
                    }

                    writer.WriteEndObject();
                }

                writer.Flush();
            }
            catch (TaskCanceledException)
            {
                throw;
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
            {
                if (ex is AggregateException aggregateException2 &&
                    aggregateException2.InnerExceptions[0] is TaskCanceledException)
                    throw aggregateException2.InnerExceptions[0];

                if (hasItem == false)
                {
                    var statusCode = _httpStatusCodeFactoryMethod(ex);
                    if (!printStatusCode)
                    {
                        writer.WritePropertyName("statusCode");
                        JToken.FromObject(statusCode, serializer).WriteTo(writer);
                    }

                    if (!printError)
                    {
                        writer.WritePropertyName("error");
                        JToken.FromObject(statusCode.GetStatusReason(), serializer).WriteTo(writer);
                    }
                }
                else
                {
                    writer.WriteEndArray();
                    if (!printError)
                    {
                        var statusCode = _httpStatusCodeFactoryMethod(ex);
                        writer.WritePropertyName("error");
                        JToken.FromObject(statusCode.GetStatusReason(), serializer).WriteTo(writer);
                    }
                }

                string message;
                string[] traces;
                var aggregateException = ex as AggregateException;
                if (aggregateException == null || aggregateException.InnerExceptions.Count == 1)
                {
                    ex = aggregateException == null ? ex : aggregateException.InnerExceptions.First();
                    message = ex.Message;
#if NETSTANDARD2_0
                    traces = ex.StackTrace.Split(Environment.NewLine.ToArray());
#else
                    traces = ex.StackTrace.Split(Environment.NewLine);
#endif
                }
                else
                {
                    message = ex.Message;
#if NETSTANDARD2_0
                    traces = ex.StackTrace.Split(Environment.NewLine.ToArray());
#else
                    traces = ex.StackTrace.Split(Environment.NewLine);
#endif
                }
                if (!printStatusCode)
                {
                    writer.WritePropertyName("message");
                    JToken.FromObject(message, serializer).WriteTo(writer);
                }
                writer.WritePropertyName("traces");
                JToken.FromObject(traces, serializer).WriteTo(writer);
                if (!printElapse)
                {
                    writer.WritePropertyName("elapse");
                    JToken.FromObject(stopWatch.Elapsed, serializer).WriteTo(writer);
                }

                writer.WriteEndObject();
                writer.Flush();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            var result = objectType != null && (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(AsyncEnumerableResponse<>));

            return result;
        }

        public override bool CanRead => false;
    }
}
