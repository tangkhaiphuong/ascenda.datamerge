using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using Newtonsoft.Json;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Net.Http
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class Response
    {
        [JsonProperty("statusCode", Order = 1)]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore, Order = 2)]
        public string Error { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore, Order = 3)]
        public string Message { get; set; }

        [JsonProperty("elapse", NullValueHandling = NullValueHandling.Ignore, Order = 4)]
        public TimeSpan? Elapse { get; set; }

        [JsonProperty("traces", NullValueHandling = NullValueHandling.Ignore, Order = 5)]
        public IEnumerable<string> Traces { get; set; }
    }

    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class Response<T> : Response
    {
        [JsonProperty("data", Order = 6)]
        public T Data { get; set; }
    }
}