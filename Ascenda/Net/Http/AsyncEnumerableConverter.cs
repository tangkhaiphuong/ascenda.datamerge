﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Net.Http
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class AsyncEnumerableConverter : JsonConverter
    {
        /// <exception cref="T:System.ArgumentNullException"><paramref name="writer"/> is <see langword="null"/></exception>
        [SuppressMessage("ReSharper", "CatchAllClause")]
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (writer == null) throw new ArgumentNullException(nameof(writer));

            writer.Flush();

            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            var startArray = false;
            try
            {
                var task = (Task)AsyncEnumerable.ForEachAsync(value as dynamic, new Action<object>(c =>

                {
                    if (startArray == false)
                    {
                        writer.WriteStartArray();
                        startArray = true;
                    }
                    if (c == null)
                    {
                        writer.WriteNull();
                        writer.Flush();
                        return;
                    }

                    var t = JToken.FromObject(c, serializer);
                    t.WriteTo(writer);
                    writer.Flush();
                }));

                task.Wait();

                if (startArray == false) writer.WriteStartArray();
                writer.WriteEndArray();

                writer.Flush();
            }
            catch (TaskCanceledException)
            {
                throw;
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
            {
                if (ex is AggregateException aggregateException2 &&
                    aggregateException2.InnerExceptions[0] is TaskCanceledException)
                    throw aggregateException2.InnerExceptions[0];

                string message;
                string[] traces;
                var aggregateException = ex as AggregateException;
                if (aggregateException == null || aggregateException.InnerExceptions.Count == 1)
                {
                    ex = aggregateException == null ? ex : aggregateException.InnerExceptions.First();
                    message = ex.Message;
#if NETSTANDARD2_0
                    traces = ex.StackTrace.Split(Environment.NewLine.ToArray());
#else
                    traces = ex.StackTrace.Split(Environment.NewLine);
#endif
                }
                else
                {
                    message = ex.Message;
#if NETSTANDARD2_0
                    traces = ex.StackTrace.Split(Environment.NewLine.ToArray());
#else
                    traces = ex.StackTrace.Split(Environment.NewLine);
#endif
                }
                var t = JToken.FromObject(message, serializer);
                t.WriteTo(writer);
                t = JToken.FromObject(traces, serializer);
                t.WriteTo(writer);
                if (startArray) writer.WriteEndArray();
                writer.Flush();
            }

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        /// <exception cref="System.Reflection.TargetInvocationException">A static initializer is invoked and throws an exception.</exception>
        /// <exception cref="System.ArgumentNullException"><paramref>source
        ///     <name>source</name>
        /// </paramref> or <paramref>predicate
        ///     <name>predicate</name>
        /// </paramref> is null.</exception>
        public override bool CanConvert(Type objectType)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));

            var result = objectType.GetInterfaces()
            .Where(i => i.IsGenericType)
            .Select(i => i.GetGenericTypeDefinition())
            .Contains(typeof(IAsyncEnumerable<>));

            return result;
        }

        public override bool CanRead => false;
    }
}
