﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

// ReSharper disable once IdentifierTypo
namespace Ascenda.Net.Http
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class AsyncEnumerableResponse<T> : Response<IAsyncEnumerable<T>>
    {
        /// <summary>
        /// Gets or sets response.
        /// </summary>
        [JsonIgnore]
        public Response Response { get; set; }
    }
}