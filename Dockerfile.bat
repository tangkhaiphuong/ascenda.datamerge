echo off
set TAG=%1
shift

docker build -t ascenda-data-merge:%TAG% .
docker tag ascenda-data-merge:%TAG% tangkhaiphuong/data-merge:%TAG%
docker push tangkhaiphuong/data-merge:%TAG%
