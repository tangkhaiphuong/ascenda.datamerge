﻿# Ascenda Data Merge

## Introduction

I complete this exercise in C# 7.3 with ASP.NET Core 2.2.
This project can build with Visual Studio 2017/2019 or VSCode with ASP.NET Core SDK 2.2 installation.

## Evaluation and Decisions

From exercise's specs. We have many data sources with a different structure. We need
merge all of them and support queries base on criteria such as the hotel id/destination.

#### Analytics:
- We have many data sources with different structures.
- Each data source may large so can't fetch & process on the fly, need fetch & process as a background job.
- Need a database for storing the result. Then it can use for querying.
- The data source can be changed without notification. So we should fetch with the interval timer.
- The application runs on server, supports Rest API and should have background job for fetching & merging.
- Merging strategy is a simple rule
  - Comparing field by field, if any null or empty, replace one by one with value.
  - If the field is array, add more items to this array with compare.
- Should deployable via Docker Image.

#### Decisions
- To transform JSON, I decide to use/write library support transform base on description same with the XLST concept. 
- So I develop 3 source transforms:
  - Transformers/Source1.json
  - Transformers/Source2.json
  - Transformers/Source3.json
- Base on these files above, it helps transfer search source to a unique format for the merging phase.

- The result of merging data need to be re-used for querying. So I decide using MongoDB for storing data.
- Each data source, I perform read each object in a stream instead of parsing whole JSON cause memory problem.
- For merge rule. Please see: `./Uility.cs` method `public static HotelEntity Merge(HotelEntity left, HotelEntity right)`

## Technicals

- `ASP.NET Core 2.2`: Start the application as a web service with REST API & Background Job
- `MongoDB`: Work as the database, store merge data and for querying hotel base on criterial
- `JUST.NET`:  For JSON transformer base on XLST concept support transform any JSON to JSON base on XSLT syntax.
- `Async/Await, Streaming`: For performance tunning on. Support concurrent processing for fetching & merging.

## Solutions Design

- Workflow.svg
- ComponentStack.svg

## Development Environment

#### Windows
- Download & install Visual Studio: https://visualstudio.microsoft.com/downloads/
- Download & install ASP.NET Core 2.2 SDK: https://dotnet.microsoft.com/download/dotnet-core/2.2
- Open project: Press F5 to debug.

#### Cross platform
- Download & install VSCode: https://code.visualstudio.com/
- Install VSCode plugins
  - ms-vscode.csharp
- Download & install ASP.NET Core 2.2 SDK: https://dotnet.microsoft.com/download/dotnet-core/2.2
- Reference: https://medium.com/danielpadua/vscode-asp-net-core-setup-development-environment-d4790d89e422
- Open project: Press F5 to debug

## Deployment

Please change the settings on your environment before running from file: 
- appsettings.json

*Note: You can change some thing*
- Mongo connection string: `"MongoUri": "mongodb://127.0.0.1:27017/ascenda"`
- Mongo collection: `"Collection": "hotels",`
- Fetching interal
- Add more source with more transformer:

```json
{
    "DataSources": [
    {
      "TransformerUrl": "file:///Transformers/Source1.json",
      "SourceUrl": "https://api.myjson.com/bins/gdmqa"
    },
    {
      "TransformerUrl": "file:///Transformers/Source2.json",
      "SourceUrl": "https://api.myjson.com/bins/1fva3m"
    },
    {
      "TransformerUrl": "file:///Transformers/Source3.json",
      "SourceUrl": "https://api.myjson.com/bins/j6kzm"
    }
  ]
}
```

#### IIS
- Publish this project.
- Create a website/application pool on IIS. Configure the binding domain.
- Copy all file from publish folder to IIS folder

#### Docker

I build the image already on: https://hub.docker.com/repository/docker/tangkhaiphuong/data-merge

##### Build image

```
docker build -t ascenda-data-merge:latest .
docker tag ascenda-data-merge:latest ascenda/data-merge:latest
docker push ascenda/data-merge:latest
```

*For my private docker hub:*

```
docker build -t ascenda-data-merge:latest .
docker tag ascenda-data-merge:latest tangkhaiphuong/data-merge:latest
docker push tangkhaiphuong/data-merge:latest
```

##### Run image


```
docker-compose up
```

## Testing
- Launch on the browser: `http://localhost:8687`
- Query hotels: `http://localhost:8687/hotels?hotel-id=iJhz&hotel-id=SjyX&destination=1122`