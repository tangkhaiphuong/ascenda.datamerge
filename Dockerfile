FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
# See: https://github.com/kleisauke/net-vips/blob/master/Dockerfile#L5

WORKDIR /app
EXPOSE 8687 80

FROM microsoft/dotnet:2.2-sdk AS build

WORKDIR /src
COPY ["Ascenda/Ascenda.csproj", "Ascenda/"]
COPY ["Ascenda.DataMerge/Ascenda.DataMerge.csproj", "Ascenda.DataMerge/"]
RUN dotnet restore "Ascenda.DataMerge/Ascenda.DataMerge.csproj"
COPY . .
WORKDIR "/src/Ascenda.DataMerge"
RUN dotnet build "Ascenda.DataMerge.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Ascenda.DataMerge.csproj" -c Release -o /app

FROM base AS final

ENV ASPNETCORE_URLS http://+:8687
WORKDIR /app

COPY --from=publish /app .

ENTRYPOINT ["dotnet", "Ascenda.DataMerge.dll"]
